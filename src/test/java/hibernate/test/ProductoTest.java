package hibernate.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;


import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ProductoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ProductoDaoImpl;

public class ProductoTest {
	
	ProductoDao ProductoDao;
		
		@Before
		public void setUp() {
			ProductoDao = new ProductoDaoImpl();

		}

		@Test
	    public void listTest() {    	
	        List<Producto> lista = ProductoDao.getProductos();        
	        //Sentencia assert para  comprobar si el dao devuelve una lista de Productoes  
	        assertTrue(lista.size() > 0);
	    }
	    
	    @Test
	    public void buscarTest() {
	        Producto Producto = ProductoDao.getProducto(1);
	        //Sentencia assert para comprobar si el método getProducto del dao  encuentra un Producto existente 
	        assertNotNull(Producto);
	    }
	    
	    @Test
	    public void insertTest() {
	    	List<Producto> lista = ProductoDao.getProductos();
	        Producto Producto = new Producto();        
	        Producto.setNombre("Prueba");
	        Producto.setTamano(35);
	        Producto.setPrecio(40);
	        Producto.setEstado("Prueba");
	        Producto.setStock(22);
	        ProductoDao.insertar(Producto);        
	        List<Producto> listaNueva = ProductoDao.getProductos();
	        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo Producto.
	        assertTrue(lista.size() < listaNueva.size());
	    }
	    
	   // @Test
	    public void deletTest() {
	    	List<Producto> lista = ProductoDao.getProductos();
	        Producto Producto = ProductoDao.getProducto(9);
	        ProductoDao.eliminar(Producto);
	        List<Producto> listaNueva = ProductoDao.getProductos();
	        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo Producto. 
	        assertTrue(lista.size() > listaNueva.size());
	    }


}
