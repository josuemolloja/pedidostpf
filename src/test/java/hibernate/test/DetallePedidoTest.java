package hibernate.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.dominio.Pedido;
import ar.edu.unju.fi.pedidos.model.dominio.DetallePedido;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.PedidoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.DetallePedidoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ProductoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ProductoDaoImpl;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.PedidoDaoImpl;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.DetallePedidoDaoImpl;

public class DetallePedidoTest {

	
DetallePedidoDao DetallePedidoDao;
PedidoDao PedidoDao;
ProductoDao ProductoDao;
	
	@Before
	public void setUp() {
		DetallePedidoDao = new DetallePedidoDaoImpl();
		PedidoDao = new PedidoDaoImpl();
		ProductoDao = new ProductoDaoImpl();

	}

	@Test
    public void listTest() {    	
        List<DetallePedido> lista = DetallePedidoDao.getDetallePedidos();        
        //Sentencia assert para  comprobar si el dao devuelve una lista de DetallePedidoes  
        assertTrue(lista.size() > 0);
    }
    
    @Test
    public void buscarTest() {
        DetallePedido DetallePedido = DetallePedidoDao.getDetallePedido(1);
        //Sentencia assert para comprobar si el método getDetallePedido del dao  encuentra un DetallePedido existente 
        assertNotNull(DetallePedido);
    }
    
    @Test
    public void insertTest() {
    	List<DetallePedido> lista = DetallePedidoDao.getDetallePedidos();
        Pedido pedido = PedidoDao.getPedido(1);
        Producto producto = ProductoDao.getProducto(1);

        DetallePedido DetallePedido = new DetallePedido();        
        DetallePedido.setPedido(pedido);
        DetallePedido.setNumItem(1);
        DetallePedido.setProducto(producto);
        DetallePedido.setPedido(pedido);
        DetallePedido.setSubtotal(350);
        DetallePedido.setCantidad(2);
        DetallePedido.setPrecioUnitario(50);
        DetallePedidoDao.insertar(DetallePedido);        
        List<DetallePedido> listaNueva = DetallePedidoDao.getDetallePedidos();
        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo DetallePedido.
        assertTrue(lista.size() < listaNueva.size());
    }
    
   // @Test
    public void deletTest() {
    	List<DetallePedido> lista = DetallePedidoDao.getDetallePedidos();
        DetallePedido DetallePedido = DetallePedidoDao.getDetallePedido(9);
        DetallePedidoDao.eliminar(DetallePedido);
        List<DetallePedido> listaNueva = DetallePedidoDao.getDetallePedidos();
        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo DetallePedido. 
        assertTrue(lista.size() > listaNueva.size());
    }

	
}
