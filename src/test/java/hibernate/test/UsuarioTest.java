package hibernate.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.dominio.Rol;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.RolDao;

import ar.edu.unju.fi.pedidos.model.dominio.Usuario;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.UsuarioDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.RolDaoImpl;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.UsuarioDaoImpl;

public class UsuarioTest {
	
UsuarioDao UsuarioDao;
RolDao rolDao;
	
	@Before
	public void setUp() {
		UsuarioDao = new UsuarioDaoImpl();
		rolDao = new RolDaoImpl();

	}

	@Test
    public void listTest() {    	
        List<Usuario> lista = UsuarioDao.getUsuarios();        
        //Sentencia assert para  comprobar si el dao devuelve una lista de Usuarioes  
        assertTrue(lista.size() > 0);
    }
    
	@Test
    public void listNombreTest() {    	
        
		List<Usuario> lista = UsuarioDao.findUsuarios("Juan");        
        //Sentencia assert para  comprobar si el dao devuelve una lista de Productoes  
        assertTrue(lista.size() > 0);
    }
	
    @Test
    public void buscarTest() {
        Usuario Usuario = UsuarioDao.getUsuario(1);
        //Sentencia assert para comprobar si el método getUsuario del dao  encuentra un Usuario existente 
        assertNotNull(Usuario);
    }
    
    @Test
    public void insertTest() {
    	List<Usuario> lista = UsuarioDao.getUsuarios();
        Usuario Usuario = new Usuario();        
        Usuario.setClave("Prueba");
        Usuario.setDni("Prueba");
        Usuario.setNombre("Prueba");
        Rol rol = rolDao.getRol(1);
        Usuario.setRol(rol);
        UsuarioDao.insertar(Usuario);        
        List<Usuario> listaNueva = UsuarioDao.getUsuarios();
        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo Usuario.
        assertTrue(lista.size() < listaNueva.size());
    }
    
   // @Test
    public void deletTest() {
    	List<Usuario> lista = UsuarioDao.getUsuarios();
        Usuario Usuario = UsuarioDao.getUsuario(9);
        UsuarioDao.eliminar(Usuario);
        List<Usuario> listaNueva = UsuarioDao.getUsuarios();
        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo Usuario. 
        assertTrue(lista.size() > listaNueva.size());
    }

}
