package hibernate.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;


import org.junit.Before;
import org.junit.Test;


import ar.edu.unju.fi.pedidos.model.dominio.Cliente;
import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ClienteDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ClienteDaoImpl;


public class ClienteTest {
	
	ClienteDao ClienteDao;
		
		@Before
		public void setUp() {
			ClienteDao = new ClienteDaoImpl();

		}

		@Test
	    public void listTest() {    	
	        List<Cliente> lista = ClienteDao.getClientes();        
	        //Sentencia assert para  comprobar si el dao devuelve una lista de Clientees  
	        assertTrue(lista.size() > 0);
	    }
		
		@Test
	    public void findNombreTest() {    	
	        List<Cliente> lista = ClienteDao.findCliente("Prueba");        
	        //Sentencia assert para  comprobar si el dao devuelve una lista de Productoes  
	        assertTrue(lista.size() > 0);
	    }
	    
	    @Test
	    public void buscarTest() {
	        Cliente Cliente = ClienteDao.getCliente(1);
	        //Sentencia assert para comprobar si el método getCliente del dao  encuentra un Cliente existente 
	        assertNotNull(Cliente);
	    }
	    
	    @Test
	    public void insertTest() {
	    	List<Cliente> lista = ClienteDao.getClientes();
	        Cliente Cliente = new Cliente();        
	        Cliente.setNombre("Prueba");
	        Cliente.setCuit(65165);
	        Cliente.setDomicilio("Prueba");
	        ClienteDao.insertar(Cliente);        
	        List<Cliente> listaNueva = ClienteDao.getClientes();
	        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo Cliente.
	        assertTrue(lista.size() < listaNueva.size());
	    }
	    
	   // @Test
	    public void deletTest() {
	    	List<Cliente> lista = ClienteDao.getClientes();
	        Cliente Cliente = ClienteDao.getCliente(9);
	        ClienteDao.eliminar(Cliente);
	        List<Cliente> listaNueva = ClienteDao.getClientes();
	        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo Cliente. 
	        assertTrue(lista.size() > listaNueva.size());
	    }
}
