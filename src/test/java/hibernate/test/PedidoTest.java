package hibernate.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.pedidos.model.dominio.Cliente;
import ar.edu.unju.fi.pedidos.model.dominio.Pedido;
import ar.edu.unju.fi.pedidos.model.dominio.Usuario;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.PedidoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.UsuarioDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ClienteDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ClienteDaoImpl;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.PedidoDaoImpl;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.UsuarioDaoImpl;

public class PedidoTest {
	
	
PedidoDao PedidoDao;
UsuarioDao UsuarioDao;
ClienteDao ClienteDao;

	
	@Before
	public void setUp() {
		PedidoDao = new PedidoDaoImpl();
		UsuarioDao = new UsuarioDaoImpl();
		ClienteDao = new ClienteDaoImpl();


	}

	@Test
    public void listTest() {    	
        List<Pedido> lista = PedidoDao.getPedidos();        
        //Sentencia assert para  comprobar si el dao devuelve una lista de Pedidoes  
        assertTrue(lista.size() > 0);
    }
	
	@Test
    public void listPorUsuarioTest() {    	
		Usuario usuario = new Usuario();
		
		
		usuario.setUsuarioId(3);
        List<Pedido> lista = PedidoDao.findPedidos(usuario);        
        //Sentencia assert para  comprobar si el dao devuelve una lista de Pedidoes  
        assertTrue(lista.size() > 0);
    }
    
    @Test
    public void buscarTest() {
        Pedido Pedido = PedidoDao.getPedido(1);
        //Sentencia assert para comprobar si el método getPedido del dao  encuentra un Pedido existente 
        assertNotNull(Pedido);
    }
    
       
    @Test
    public void insertTest() {
    	List<Pedido> lista = PedidoDao.getPedidos();       
        Pedido Pedido = new Pedido();
        Usuario Usuario = UsuarioDao.getUsuario(1);
        Cliente Cliente = ClienteDao.getCliente(1);

        Pedido.setCliente(Cliente);
        Pedido.setUsuario(Usuario);
        Pedido.setFecha(null);
        Pedido.setImporte(2000);
        Pedido.setEstado("Pendiente");
        PedidoDao.insertar(Pedido);        
        List<Pedido> listaNueva = PedidoDao.getPedidos();
        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo Pedido.
        assertTrue(lista.size() < listaNueva.size());
    }
    
   // @Test
    public void deletTest() {
    	List<Pedido> lista = PedidoDao.getPedidos();
        Pedido Pedido = PedidoDao.getPedido(9);
        PedidoDao.eliminar(Pedido);
        List<Pedido> listaNueva = PedidoDao.getPedidos();
        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo Pedido. 
        assertTrue(lista.size() > listaNueva.size());
    }


}
