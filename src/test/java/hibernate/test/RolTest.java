/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hibernate.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.pedidos.model.dominio.Rol;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.RolDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.RolDaoImpl;

/**
 *
 * @author José Zapana
 */
public class RolTest {
	
	RolDao rolDao;
	
	@Before
	public void setUp() {
		rolDao = new RolDaoImpl();
	}

	@Test
    public void listTest() {    	
        List<Rol> lista = rolDao.getRoles();        
        //Sentencia assert para  comprobar si el dao devuelve una lista de roles  
        assertTrue(lista.size() > 0);
    }
    
    @Test
    public void buscarTest() {
        Rol rol = rolDao.getRol(1);
        //Sentencia assert para comprobar si el método getRol del dao  encuentra un rol existente 
        assertNotNull(rol);
    }
    
    @Test
    public void insertTest() {
    	List<Rol> lista = rolDao.getRoles();
        Rol rol = new Rol();        
        rol.setDescripcion("Prueba");
        rolDao.insertar(rol);        
        List<Rol> listaNueva = rolDao.getRoles();
        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo rol. 
        assertTrue(lista.size() < listaNueva.size());
    }
    
   // @Test
    public void deletTest() {
    	List<Rol> lista = rolDao.getRoles();
        Rol rol = rolDao.getRol(9);
        rolDao.eliminar(rol);
        List<Rol> listaNueva = rolDao.getRoles();
        //Sentencia assert para comprobar si el método insertar del dao  esta persistiendo un objeto de tipo rol. 
        assertTrue(lista.size() > listaNueva.size());
    }
    
}
