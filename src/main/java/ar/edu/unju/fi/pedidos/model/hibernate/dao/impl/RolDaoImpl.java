package ar.edu.unju.fi.pedidos.model.hibernate.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

import ar.edu.unju.fi.pedidos.model.dominio.Rol;
import ar.edu.unju.fi.pedidos.model.hibernate.base.HibernateBase;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.RolDao;

/**
 *
 * @author Jos� Zapana
 */
public class RolDaoImpl extends HibernateBase implements RolDao{


    
	@SuppressWarnings("unchecked")
	public List<Rol> getRoles(){
        Criteria criteria = getSession().createCriteria(Rol.class);
        criteria.addOrder(Order.asc("descripcion"));
        return criteria.list();
    }


	public void insertar(Rol rol)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().save(rol);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }



	public Rol getRol(int id){
        getSession().beginTransaction();
        return (Rol)getSession().get(Rol.class, id);
    }
    

	public void actualizar(Rol rol)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().update(rol);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }
	
//Agregu� yo
	public void eliminar(Rol rol)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().delete(rol);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }
	

	
    
}
