package ar.edu.unju.fi.pedidos.model.hibernate.dao;

import java.util.List;

import ar.edu.unju.fi.pedidos.model.dominio.Rol;

public interface RolDao {

	List<Rol> getRoles();

	void insertar(Rol rol);

	/**
	 * Actualiza un rol
	 * @param rol
	 */
	void actualizar(Rol rol);

	Rol getRol(int id);
	
	//Agregu� yo
	void eliminar(Rol rol);


}