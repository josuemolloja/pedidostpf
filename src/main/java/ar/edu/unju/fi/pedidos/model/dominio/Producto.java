package ar.edu.unju.fi.pedidos.model.dominio;

import java.util.HashSet;
import java.util.Set;

public class Producto implements java.io.Serializable {
	
	private static final long serialVersionUID = -5569602158363800498L;
	
	private Integer productoId;
    private String nombre;
    private int tamano;
    private float precio;
    private String estado;
    private int stock;
    
    private Set<DetallePedido> detallePedido = new HashSet<DetallePedido>(0);


    
    public Producto() {}
    
    public Producto (String nombre, int tamano, float precio, String estado, int stock) {
    	
    	this.nombre = nombre;
    	this.tamano = tamano;
    	this.estado = estado;
    	this.stock = stock;
    	
    }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getTamano() {
		return tamano;
	}

	public void setTamano(int tamano) {
		this.tamano = tamano;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Integer getProductoId() {
		return productoId;
	}

	public void setProductoId(Integer productoId) {
		this.productoId = productoId;
	}

    public Set<DetallePedido> getdetallePedido() {
        return this.detallePedido;
    }
    
    public void setdetallePedido(Set<DetallePedido> detalles) {
        this.detallePedido = detalles;
    }
    
}
