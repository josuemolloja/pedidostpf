package ar.edu.unju.fi.pedidos.view.controller.beans.seguridad;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import ar.edu.unju.fi.pedidos.model.dominio.Usuario;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.UsuarioDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.UsuarioDaoImpl;



/**
 * 
 * Bean para el login del sistema
 * @author Jos� Zapana
 *
 */
@ManagedBean(name="loginBean")
@SessionScoped
public class LoginBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 
	private String dni;
	private String password;
	private final String FORWARD_DASHBOARD = "escritorio.xhtml?faces-redirect=true";
	private UsuarioDao usuarioDao = new UsuarioDaoImpl();

	/**
	 * Login del sistema
	 * @return
	 * @throws Exception 
	 */
	public String ingresar() throws Exception{
System.out.println("ingresando....");
			if(dni.isEmpty() || password.isEmpty()){
				
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Debe Ingresar usuario y clave"));
				return null;
			}
			Usuario usuario = usuarioDao.validarUsuario(dni, password);
			System.out.println(usuario);
			if ( usuario == null) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "No existe un usuario con esos datos..."));
				return null;
			}else			
				return FORWARD_DASHBOARD;
	
	}

	/**
	 * Cerrar sesi�n
	 * @return
	 */
	public void closeSession() {
		
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(false);
		session.invalidate();


	}
	public void cambiarClave() {
		System.out.println("cambiando clave....");
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	
	
}
