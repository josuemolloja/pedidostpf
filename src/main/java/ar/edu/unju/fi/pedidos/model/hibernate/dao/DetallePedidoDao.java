package ar.edu.unju.fi.pedidos.model.hibernate.dao;

import java.util.List;

import ar.edu.unju.fi.pedidos.model.dominio.DetallePedido;


public interface DetallePedidoDao {
		
	
	void insertar(DetallePedido DetallePedido);
	
	void actualizar(DetallePedido DetallePedido);

	DetallePedido getDetallePedido(int id);
	
	void eliminar(DetallePedido DetallePedido);
	
	List<DetallePedido> getDetallePedidos(); 


}
