package ar.edu.unju.fi.pedidos.view.controller.beans.seguridad;


import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;

import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.dominio.Rol;
import ar.edu.unju.fi.pedidos.model.dominio.Usuario;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ProductoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.UsuarioDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ProductoDaoImpl;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.UsuarioDaoImpl;


@ManagedBean
@SessionScoped
public class UsuarioListBean implements Serializable {
	
	 private static Logger logger = Logger.getLogger(UsuarioListBean.class);
		/**
		 * 
		 */
		private static final long serialVersionUID = -8194186654346103799L;
		private Integer usuarioId;
		private String nombre;
		private String dni;
		private Rol rol;
		
		private List<Usuario> usuarioList;
		
		public UsuarioListBean() {
			
			UsuarioDao dao = new UsuarioDaoImpl();
			
			this.usuarioList = dao.getUsuarios();
		}

		public Integer getUsuarioId() {
			return usuarioId;
		}

		public void setUsuarioId(Integer usuarioId) {
			this.usuarioId = usuarioId;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getDni() {
			return dni;
		}

		public void setDni(String dni) {
			this.dni = dni;
		}

		public Rol getRol() {
			return rol;
		}

		public void setRol(Rol rol) {
			this.rol = rol;
		}

		public List<Usuario> getUsuarioList() {
			return usuarioList;
		}

		public void setUsuarioList(List<Usuario> usuarioList) {
			this.usuarioList = usuarioList;
		}

		/**
		 * B�squeda de Usuarios
		 * @return
		 */
		public String search(){
			UsuarioDao dao = new UsuarioDaoImpl();
			
			//FIXME aqu� deber�a implementarse la b�squeda con par�metros
			logger.debug("usuarioId: " + usuarioId + "  - nombre: " + nombre);
			
			usuarioList = dao.findUsuarios(nombre);
			logger.debug("...... productList: " + usuarioList.toString());
			
			
//			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Buscando Productos...", "Se realiz� la b�squeda de productos");
//	        FacesContext.getCurrentInstance().addMessage(null, message);
			
			return null;
		}
		
		

				
		
		/**
		 * Guarda el objeto modificado
		 * 
		 * @param event
		 * @author jzapana
		 * @version 1.0
		 * @since 10/05/2016
		 */
		/*public void editarProducto(RowEditEvent event) {

			Producto product = (Producto) event.getObject();
			System.out.println("modificando precio del producto....: "+product.getPrecio());
			logger.debug("modificando precio del producto....: "+product.getPrecio());
			
		}*/




		/**
		 * Cancela la modificaci�n
		 * 
		 * @param event
		 * @author jzapana
		 * @version 1.0
		 * @since 10/05/2016
		 */
		/*public void cancelarEdicion(RowEditEvent event) {
			//addLocalizedWarningMessage("bundle", "message.cancel.success");
			logger.debug("cancelando ....");
			System.out.println("cancelando....");
			
		}*/
		
		

		

}
