package ar.edu.unju.fi.pedidos.model.hibernate.dao;

import java.util.List;

import ar.edu.unju.fi.pedidos.model.dominio.Cliente;

public interface ClienteDao {

	List<Cliente> getClientes();

	void insertar(Cliente cliente);

	List<Cliente> findCliente(String nombre);

	void actualizar(Cliente cliente);

	Cliente getCliente(int id);
	
	void eliminar(Cliente cliente);

}
