package ar.edu.unju.fi.pedidos.model.hibernate.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.edu.unju.fi.pedidos.model.dominio.Cliente;
import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.hibernate.base.HibernateBase;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ClienteDao;

public class ClienteDaoImpl extends HibernateBase implements ClienteDao{

	@SuppressWarnings("unchecked")
	public List<Cliente> getClientes() {
		 Criteria criteria = getSession().createCriteria(Cliente.class);
	        criteria.addOrder(Order.asc("nombre"));
	        return criteria.list();
	}

	public void insertar(Cliente cliente) {
		try{
	          Transaction transaction = getSession().beginTransaction();
	          getSession().save(cliente);
	          transaction.commit();
	       }catch(HibernateException e){
	           e.printStackTrace();
	       }
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Cliente> findCliente(String nombre){
        Criteria criteria = getSession().createCriteria(Cliente.class);
		criteria.add(Restrictions.ilike("nombre", "%"+nombre+"%"));
        criteria.addOrder(Order.asc("nombre"));
		
        return criteria.list();
    }

	public void actualizar(Cliente cliente) {
		// TODO Auto-generated method stub
		try{
	          Transaction transaction = getSession().beginTransaction();
	          getSession().update(cliente);
	          transaction.commit();
	       }catch(HibernateException e){
	           e.printStackTrace();
	       }
	}

	public Cliente getCliente(int id) {
		getSession().beginTransaction();
        return (Cliente)getSession().get(Cliente.class, id);
	}
	
	//Agregu� yo
		public void eliminar(Cliente Cliente)
	    {
	       try{
	          Transaction transaction = getSession().beginTransaction();
	          getSession().delete(Cliente);
	          transaction.commit();
	       }catch(HibernateException e){
	           e.printStackTrace();
	       }
	    }

	
}
