package ar.edu.unju.fi.pedidos.model.dominio;
// Generated Jun 29, 2010 11:06:30 PM by Hibernate Tools 3.2.1.GA



/**
 * Usuarios generated by hbm2java
 */
public class Usuario  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = -5569602158363800498L;
	 private Integer usuarioId;
	 private String dni;
     private Rol rol;
     private String clave;
     private String nombre;

    public Usuario() {
    }

    public Usuario(String dni, Rol roles, String clave, String nombre) {
       this.dni = dni;
       this.rol = roles;
       this.clave = clave;
       this.nombre = nombre;
    }
   
    public Integer getUsuarioId() {
        return this.usuarioId;
    }
    
    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }
    public Rol getRol() {
        return this.rol;
    }
    
    public void setRol(Rol rol) {
        this.rol = rol;
    }
    public String getClave() {
        return this.clave;
    }
    
    public void setClave(String clave) {
        this.clave = clave;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

}


