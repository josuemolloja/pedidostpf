package ar.edu.unju.fi.pedidos.model.hibernate.dao;

import java.util.List;

import ar.edu.unju.fi.pedidos.model.dominio.Pedido;
import ar.edu.unju.fi.pedidos.model.dominio.Usuario;

public interface PedidoDao {
	
	void insertar(Pedido Pedido);
	
	void actualizar(Pedido Pedido);

	Pedido getPedido(int id);
	
	void eliminar(Pedido Pedido);
	
	List<Pedido> getPedidos(); 
	List<Pedido> findPedidos(Usuario usuario); 
	List<Usuario> getUsuarios(); 



}
