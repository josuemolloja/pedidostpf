package ar.edu.unju.fi.pedidos.view.controller.beans.seguridad;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ProductoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ProductoDaoImpl;


@ManagedBean
@SessionScoped
public class ProductAltaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Producto producto;
	private String nombre;
	private int tamano;
    private float precio;
    private String estado;
    private int stock;
    
    

	


	public int getTamano() {
		return tamano;
	}

	public void setTamano(int tamano) {
		this.tamano = tamano;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Producto getProducto() {
		return producto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ProductAltaBean(){
		
	}
	
	public String preInsert(){
		System.out.println("******** preInsert");
		setProducto(new Producto());
		return "productAlta.xhtml?faces-redirect=true";
	}	
	
	/**
	 * sucede antes de entrar a la pantalla de modificaci�n
	 * @return
	 */
	public String preUpdate(){
		System.out.println("******** preUpdate alta - C�digo: " + getProduct().getProductoId() + " Nombre: "+ getProduct().getNombre());
		return "productAlta.xhtml?faces-redirect=true";
	}	
	
	public Producto getProduct() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	public String guardar(){
		System.out.println("...... guardando....");
		
        Producto Producto = new Producto(); 
		ProductoDao ProductoDao = new ProductoDaoImpl();
        Producto.setNombre(nombre);
        Producto.setTamano(tamano);
        Producto.setPrecio(precio);
        Producto.setEstado(estado);
        Producto.setStock(stock);
        ProductoDao.insertar(Producto);  
        
		//lista.add(product);  Guardo el objeto en la lista est�tica
		return "productList";
	}
}
