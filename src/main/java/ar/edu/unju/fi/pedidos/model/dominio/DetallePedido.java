package ar.edu.unju.fi.pedidos.model.dominio;


public class DetallePedido implements java.io.Serializable {
	
	private static final long serialVersionUID = -5569602158363800498L;

	private Integer detalleId;
    private Pedido pedido;
    private Integer numItem;
    private Producto producto;
    private float subtotal;
    private float precioUnitario;
    private Integer cantidad;

    
    public DetallePedido() {}
    
    public DetallePedido(Pedido pedido, int numItem, Producto producto, float subtotal, float precioUnitario, Integer cantidad) {
    	
    	this.pedido = pedido;
    	this.numItem = numItem;
    	this.producto = producto;
    	this.subtotal = subtotal;
    	this.precioUnitario = precioUnitario;
    	this.cantidad = cantidad;
    	
    }

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Integer getNumItem() {
		return numItem;
	}

	public void setNumItem(Integer numItem) {
		this.numItem = numItem;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public float getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(float subtotal) {
		this.subtotal = subtotal;
	}

	public Integer getDetalleId() {
		return detalleId;
	}
	
	public void setDetalleId(Integer detalleId) {
		this.detalleId = detalleId;
	}

	public float getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(float precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
    
   
}
