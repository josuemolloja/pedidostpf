package ar.edu.unju.fi.pedidos.model.dominio;

import java.util.HashSet;
import java.util.Set;

public class Cliente implements java.io.Serializable {

	private static final long serialVersionUID = -5569602158363800498L;

	private Integer clienteId;
    private Integer cuit;
    private String nombre;
    private String domicilio;
    
	private Set<Pedido> pedidos = new HashSet<Pedido>(0);

    
    public Cliente() {}

    
    public Cliente (int cuit, String nombre, String domicilio ) {
    	
    	this.cuit = cuit;
    	this.nombre = nombre;
    	this.domicilio = domicilio;
    	
    }

	public Integer getClienteId() {
		return clienteId;
	}
	
	 public void setClienteId(Integer clienteId) {
	        this.clienteId = clienteId;
	    }

	public Integer getCuit() {
		return cuit;
	}

	public void setCuit(Integer cuit) {
		this.cuit = cuit;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	
	public Set<Pedido> getPedidos() {
        return this.pedidos;
    }
	
	  public void setPedidos(Set<Pedido> pedidos) {
			this.pedidos = pedidos;
		}

	
}
