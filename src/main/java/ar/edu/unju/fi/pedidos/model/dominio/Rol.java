package ar.edu.unju.fi.pedidos.model.dominio;
// Generated Jun 29, 2010 11:06:30 PM by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;

/**
 * @author Jose Zapana
 */
public class Rol  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = -1250922252640316631L;
	private int rolId;
     private String descripcion;
     private Set<Usuario> usuarios = new HashSet<Usuario>(0);

    public Rol() {
    }

	
    public Rol(int rolId, String descripcion) {
        this.rolId = rolId;
        this.descripcion = descripcion;
    }
    public Rol(int rolId, String descripcion, Set<Usuario> usuarioses) {
       this.rolId = rolId;
       this.descripcion = descripcion;
       this.usuarios = usuarioses;
    }
   
    public int getRolId() {
        return this.rolId;
    }
    
    public void setRolId(int rolId) {
        this.rolId = rolId;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public Set<Usuario> getUsuarios() {
        return this.usuarios;
    }
    
    public void setUsuarios(Set<Usuario> usuarioses) {
        this.usuarios = usuarioses;
    }

}


