package ar.edu.unju.fi.pedidos.view.controller.beans.seguridad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ar.edu.unju.fi.pedidos.model.dominio.Cliente;
import ar.edu.unju.fi.pedidos.model.dominio.DetallePedido;
import ar.edu.unju.fi.pedidos.model.dominio.Pedido;
import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ClienteDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ProductoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ClienteDaoImpl;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ProductoDaoImpl;


@ManagedBean
@SessionScoped
public class PedidoAltaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
    private List<DetallePedido> listaDetalle;
    private List<Cliente> listCliente;
    private Cliente cliente;
    

	public List<Cliente> getListCliente() {
		return listCliente;
	}


	public void setListCliente(List<Cliente> listCliente) {
		this.listCliente = listCliente;
	}


	public Cliente getCliente() {
		return cliente;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


	public PedidoAltaBean() 
	{
		
		ClienteDao clienteDao = new ClienteDaoImpl();
		listCliente = new ArrayList<Cliente>();
		
		Cliente cliente1 = new Cliente();
		cliente1.setNombre("Josue");
		
		listCliente.add(cliente1);
		
		listaDetalle = new ArrayList<DetallePedido>();
		
		Pedido pedido = new Pedido();
		Producto producto = new Producto();
		
		producto.setProductoId(32);
		producto.setNombre("Coquita");
		producto.setTamano(55);
		
		listaDetalle.add(new DetallePedido(pedido, 123, producto, 5*3, 5, 3));
	}


	public List<DetallePedido> getListaDetalle() {
		return listaDetalle;
	}


	public void setListaDetalle(List<DetallePedido> listaDetalle) {
		this.listaDetalle = listaDetalle;
	}


	public String preInsert(){
		/*System.out.println("******** preInsert");
		setProducto(new Producto());*/
		return "pedidoAlta.xhtml?faces-redirect=true";
	}	
	
	
}
