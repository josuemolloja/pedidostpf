package ar.edu.unju.fi.pedidos.view.controller.beans.seguridad;


import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;

import ar.edu.unju.fi.pedidos.model.dominio.Cliente;
import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.dominio.Rol;
import ar.edu.unju.fi.pedidos.model.dominio.Usuario;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ClienteDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ProductoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.UsuarioDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ClienteDaoImpl;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ProductoDaoImpl;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.UsuarioDaoImpl;


@ManagedBean
@SessionScoped
public class ClienteListBean implements Serializable {
	
	 private static Logger logger = Logger.getLogger(ClienteListBean.class);
		/**
		 * 
		 */
		private static final long serialVersionUID = -8194186654346103799L;
		private Integer clienteId;
		private String nombre;
		private Integer cuit;
		private String domicilio;
		
		private List<Cliente> clienteList;
		
		public ClienteListBean() {
			
			ClienteDao dao = new ClienteDaoImpl();
			
			this.clienteList = dao.getClientes();
		}

		/**
		 * B�squeda de Usuarios
		 * @return
		 */
		public String search(){
			ClienteDao dao = new ClienteDaoImpl();
			
			//FIXME aqu� deber�a implementarse la b�squeda con par�metros
			logger.debug("usuarioId: " + clienteId + "  - nombre: " + nombre);
			
			clienteList = dao.findCliente(nombre);
			logger.debug("...... productList: " + clienteList.toString());
			
			
//			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Buscando Productos...", "Se realiz� la b�squeda de productos");
//	        FacesContext.getCurrentInstance().addMessage(null, message);
			
			return null;
		}

		public Integer getClienteId() {
			return clienteId;
		}

		public void setClienteId(Integer clienteId) {
			this.clienteId = clienteId;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public Integer getCuit() {
			return cuit;
		}

		public void setCuit(Integer cuit) {
			this.cuit = cuit;
		}

		public String getDomicilio() {
			return domicilio;
		}

		public void setDomicilio(String domicilio) {
			this.domicilio = domicilio;
		}

		public List<Cliente> getClienteList() {
			return clienteList;
		}

		public void setClienteList(List<Cliente> clienteList) {
			this.clienteList = clienteList;
		}
		
		

				
		
		/**
		 * Guarda el objeto modificado
		 * 
		 * @param event
		 * @author jzapana
		 * @version 1.0
		 * @since 10/05/2016
		 */
		/*public void editarProducto(RowEditEvent event) {

			Producto product = (Producto) event.getObject();
			System.out.println("modificando precio del producto....: "+product.getPrecio());
			logger.debug("modificando precio del producto....: "+product.getPrecio());
			
		}*/




		/**
		 * Cancela la modificaci�n
		 * 
		 * @param event
		 * @author jzapana
		 * @version 1.0
		 * @since 10/05/2016
		 */
		/*public void cancelarEdicion(RowEditEvent event) {
			//addLocalizedWarningMessage("bundle", "message.cancel.success");
			logger.debug("cancelando ....");
			System.out.println("cancelando....");
			
		}*/
		
		

		

}
