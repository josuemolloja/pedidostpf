package ar.edu.unju.fi.pedidos.view.controller.beans.seguridad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;

import ar.edu.unju.fi.pedidos.model.dominio.Pedido;
import ar.edu.unju.fi.pedidos.model.dominio.Usuario;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.PedidoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.PedidoDaoImpl;


@ManagedBean
@SessionScoped
public class PedidoListBean implements Serializable {
	
    private static Logger logger = Logger.getLogger(ProductListBean.class);

	
	private static final long serialVersionUID = -8194186654346103799L;
	private Integer pedidoId;
	private List<Pedido>pedidoList;
	private Usuario usuario;
	
	private List<Usuario>usuariosList;
	
	private List<SelectItem> listUsuarios;

	
	public PedidoListBean() {
		
		PedidoDao dao = new PedidoDaoImpl();
		
		this.pedidoList = dao.getPedidos();
		
		usuario = new Usuario();
	}

	/**
	 * B�squeda de pedidos
	 * @return
	 */
	public String search(){
		PedidoDao dao = new PedidoDaoImpl();
		
		//FIXME aqu� deber�a implementarse la b�squeda con par�metros
		logger.debug("pedidoId: " + pedidoId);
		
		pedidoList = dao.findPedidos(usuario);
		logger.debug("...... pedidoList: " + pedidoList.size());
		
		
//		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Buscando pedidoos...", "Se realiz� la b�squeda de pedidoos");
//        FacesContext.getCurrentInstance().addMessage(null, message);
		
		return null;
	}
	
	
	/**
	 * Guarda el objeto modificado
	 * 
	 * @param event
	 * @author jzapana
	 * @version 1.0
	 * @since 10/05/2016
	 */
	public void editarpedido(RowEditEvent event) {

		Pedido pedido = (Pedido) event.getObject();
		System.out.println("modificando precio del pedidoo....: "+pedido.getEstado());
		logger.debug("modificando precio del pedidoo....: "+pedido.getEstado());
		
	}




	/**
	 * Cancela la modificaci�n
	 * 
	 * @param event
	 * @author jzapana
	 * @version 1.0
	 * @since 10/05/2016
	 */
	public void cancelarEdicion(RowEditEvent event) {
		//addLocalizedWarningMessage("bundle", "message.cancel.success");
		logger.debug("cancelando ....");
		System.out.println("cancelando....");
		
	}
	
	
	
	   public List<Usuario> buscarUsuarios(String query) {
		   
			PedidoDao dao = new PedidoDaoImpl();
			
			usuariosList = dao.getUsuarios();


		 /*  
	        List<String> results = new ArrayList<String>();
	        for(int i = 0; i < 10; i++) {
	            results.add(query + i);
	        }
	       */  
	        return usuariosList;
	    }
	

	public Integer getPedidoId() {
		return pedidoId;
	}
	public void setPedidoId(Integer pedidooId) {
		this.pedidoId = pedidooId;
	}
	public List<Pedido> getPedidoList() {
		return pedidoList;
	}
	public void setPedidoList(List<Pedido> pedidoList) {
		this.pedidoList = pedidoList;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<SelectItem> getListUsuarios() {
		
		this.listUsuarios = new ArrayList<SelectItem>();
		PedidoDao uDao = new PedidoDaoImpl();
		
		List<Usuario> u = uDao.getUsuarios();
		listUsuarios.clear();
		
		for (Usuario usuarios : u) {
			SelectItem usuarioItem = new SelectItem(usuarios.getUsuarioId(), usuarios.getNombre());
			this.listUsuarios.add(usuarioItem);
		}
		
		return listUsuarios;
	}

	public void setListUsuarios(List<SelectItem> listUsuarios) {
		this.listUsuarios = listUsuarios;
	}
	

}
