package ar.edu.unju.fi.pedidos.model.hibernate.dao;

import java.util.List;

import ar.edu.unju.fi.pedidos.model.dominio.Producto;


public interface ProductoDao {
	
	
	void insertar(Producto Producto);
	
	void actualizar(Producto Producto);

	Producto getProducto(int id);
	
	//Agregu� yo
	void eliminar(Producto Producto);
	
	List<Producto> getProductos(); 
	List<Producto> findProductos(String nombre); 


	
}
