package ar.edu.unju.fi.pedidos.model.hibernate.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.dominio.Usuario;
import ar.edu.unju.fi.pedidos.model.hibernate.base.HibernateBase;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.UsuarioDao;

public class UsuarioDaoImpl extends HibernateBase implements UsuarioDao {

	/**
	 * Validaci�n de usuarios
	 */
	public Usuario validarUsuario(String dni, String clave) {
        Criteria criteria = getSession().createCriteria(Usuario.class);
        criteria.add(Restrictions.eq("dni",dni));
        criteria.add(Restrictions.eq("clave",clave));
		return (Usuario)criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> findUsuarios(String nombre){
        Criteria criteria = getSession().createCriteria(Usuario.class);
		criteria.add(Restrictions.ilike("nombre", "%"+nombre+"%"));
        criteria.addOrder(Order.asc("nombre"));
		
        return criteria.list();
    }
	
	@SuppressWarnings("unchecked")
	public List<Usuario> getUsuarios(){
        Criteria criteria = getSession().createCriteria(Usuario.class);
        criteria.addOrder(Order.asc("nombre"));
        return criteria.list();
    }

	
	public void insertar(Usuario usuario)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().save(usuario);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }
	

	public Usuario getUsuario(int id){
        getSession().beginTransaction();
        return (Usuario)getSession().get(Usuario.class, id);
    }
    

	public void actualizar(Usuario Usuario)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().update(Usuario);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }
	
	public void eliminar(Usuario Usuario)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().delete(Usuario);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }

	
	
}
