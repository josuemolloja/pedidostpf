package ar.edu.unju.fi.pedidos.model.hibernate.dao;

import java.util.List;

import ar.edu.unju.fi.pedidos.model.dominio.Usuario;

public interface UsuarioDao {

	Usuario validarUsuario(String dni, String clave);
	
	//Agregu� yo
	
	List<Usuario> findUsuarios(String nombre);
	
	void insertar(Usuario usuario);
	
	void actualizar(Usuario usuario);

	Usuario getUsuario(int id);
	
	//Agregu� yo
	void eliminar(Usuario usuario);
	
	List<Usuario> getUsuarios(); 


}