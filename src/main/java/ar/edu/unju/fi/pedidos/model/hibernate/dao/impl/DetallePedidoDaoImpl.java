package ar.edu.unju.fi.pedidos.model.hibernate.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

import ar.edu.unju.fi.pedidos.model.dominio.DetallePedido;
import ar.edu.unju.fi.pedidos.model.hibernate.base.HibernateBase;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.DetallePedidoDao;

public class DetallePedidoDaoImpl extends HibernateBase implements DetallePedidoDao
{

	
	
	@SuppressWarnings("unchecked")
	public List<DetallePedido> getDetallePedidos(){
        Criteria criteria = getSession().createCriteria(DetallePedido.class);
        criteria.addOrder(Order.asc("detalleId"));
        return criteria.list();
    }
	
	public void insertar(DetallePedido DetallePedido)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().save(DetallePedido);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }
	

	public DetallePedido getDetallePedido(int id){
        getSession().beginTransaction();
        return (DetallePedido)getSession().get(DetallePedido.class, id);
    }
    

	public void actualizar(DetallePedido DetallePedido)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().update(DetallePedido);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }
	
	public void eliminar(DetallePedido DetallePedido)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().delete(DetallePedido);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }

	
	
}
