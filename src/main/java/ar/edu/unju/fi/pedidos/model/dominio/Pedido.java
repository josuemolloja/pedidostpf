package ar.edu.unju.fi.pedidos.model.dominio;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Pedido implements java.io.Serializable {
	
	private static final long serialVersionUID = -5569602158363800498L;

	private Integer pedidoId;
    private Usuario usuario;
    private Cliente cliente;
    private Date fecha;
    private float importe;
    private String estado;
    
    private Set<DetallePedido> detallePedido = new HashSet<DetallePedido>(0);

    
    public Pedido() {}
    
 public Pedido(Usuario usuario, Cliente cliente, Date fecha, float importe, String estado ) {
    	
	 	this.usuario = usuario;
	 	this.cliente = cliente;
	 	this.fecha = fecha;
	 	this.importe = importe;
	 	this.estado = estado;
    }

	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Date getFecha() {
		return fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public float getImporte() {
		return importe;
	}
	
	public void setImporte(float importe) {
		this.importe = importe;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public Integer getPedidoId() {
		return pedidoId;
	}
	
	public void setPedidoId(Integer pedidoId) {
		this.pedidoId = pedidoId;
	}

    public Set<DetallePedido> getDetallePedido() {
        return this.detallePedido;
    }
    
    public void setDetallePedido(Set<DetallePedido> detallePedido) {
        this.detallePedido = detallePedido;
    }
	    
	 	

}
