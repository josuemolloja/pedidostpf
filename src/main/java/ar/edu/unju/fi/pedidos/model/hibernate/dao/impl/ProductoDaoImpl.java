package ar.edu.unju.fi.pedidos.model.hibernate.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.hibernate.base.HibernateBase;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ProductoDao;

public class ProductoDaoImpl extends HibernateBase implements ProductoDao {

	
	@SuppressWarnings("unchecked")
	public List<Producto> getProductos(){
        Criteria criteria = getSession().createCriteria(Producto.class);
        criteria.addOrder(Order.asc("nombre"));
        return criteria.list();
    }

	@SuppressWarnings("unchecked")
	public List<Producto> findProductos(String nombre){
        Criteria criteria = getSession().createCriteria(Producto.class);
		criteria.add(Restrictions.ilike("nombre", "%"+nombre+"%"));
        criteria.addOrder(Order.asc("nombre"));
		
        return criteria.list();
    }
	
	public void insertar(Producto Producto)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().save(Producto);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }
	

	public Producto getProducto(int id){
        getSession().beginTransaction();
        return (Producto)getSession().get(Producto.class, id);
    }
    

	public void actualizar(Producto Producto)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().update(Producto);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }
	
	public void eliminar(Producto Producto)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().delete(Producto);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }

	
	
}
