package ar.edu.unju.fi.pedidos.model.hibernate.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.edu.unju.fi.pedidos.model.dominio.Pedido;
import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.dominio.Rol;
import ar.edu.unju.fi.pedidos.model.dominio.Usuario;
import ar.edu.unju.fi.pedidos.model.hibernate.base.HibernateBase;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.PedidoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.UsuarioDao;

public class PedidoDaoImpl extends HibernateBase implements PedidoDao {
	
	
	@SuppressWarnings("unchecked")
	public List<Pedido> getPedidos(){
        Criteria criteria = getSession().createCriteria(Pedido.class);
        criteria.addOrder(Order.asc("fecha"));
        return criteria.list();
    }
	
	@SuppressWarnings("unchecked")
	public List<Pedido> findPedidos(Usuario usuario){
               
		Criteria criteria = getSession().createCriteria(Pedido.class).addOrder(Order.asc("pedidoId")).createCriteria("usuario");
		criteria.add(Restrictions.eq("usuarioId",usuario.getUsuarioId()));
		//criteria.add(Restrictions.like("usuarioId", usuario.getUsuarioId()));

        return criteria.list();
    }
	
	@SuppressWarnings("unchecked")
	public List<Usuario> getUsuarios(){
        Criteria criteria = getSession().createCriteria(Usuario.class).addOrder(Order.asc("nombre")).createCriteria("rol");
		criteria.add(Restrictions.like("descripcion", "Vendedor%"));
        return criteria.list();
    }

	
	public void insertar(Pedido Pedido)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().save(Pedido);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }
	

	public Pedido getPedido(int id){
        getSession().beginTransaction();
        return (Pedido)getSession().get(Pedido.class, id);
    }
    

	public void actualizar(Pedido Pedido)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().update(Pedido);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }
	
	public void eliminar(Pedido Pedido)
    {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().delete(Pedido);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
       }
    }

	


}
