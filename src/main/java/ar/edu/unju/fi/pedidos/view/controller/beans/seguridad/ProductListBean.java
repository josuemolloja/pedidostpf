package ar.edu.unju.fi.pedidos.view.controller.beans.seguridad;


import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;

import ar.edu.unju.fi.pedidos.model.dominio.Producto;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.ProductoDao;
import ar.edu.unju.fi.pedidos.model.hibernate.dao.impl.ProductoDaoImpl;


@ManagedBean
@SessionScoped
public class ProductListBean implements Serializable {
	
	 private static Logger logger = Logger.getLogger(ProductListBean.class);
		/**
		 * 
		 */
		private static final long serialVersionUID = -8194186654346103799L;
		private Integer productoId;
		private String nombre;
		private List<Producto>productoList;
		
		public ProductListBean() {
			
			ProductoDao dao = new ProductoDaoImpl();
			
			this.productoList = dao.getProductos();
		}

		/**
		 * B�squeda de productos
		 * @return
		 */
		public String search(){
			ProductoDao dao = new ProductoDaoImpl();
			
			//FIXME aqu� deber�a implementarse la b�squeda con par�metros
			logger.debug("productoId: " + productoId + "  - nombre: " + nombre);
			
			productoList = dao.findProductos(nombre);
			logger.debug("...... productList: " + productoList.size());
			
			
//			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Buscando Productos...", "Se realiz� la b�squeda de productos");
//	        FacesContext.getCurrentInstance().addMessage(null, message);
			
			return null;
		}
		
		
		/**
		 * Guarda el objeto modificado
		 * 
		 * @param event
		 * @author jzapana
		 * @version 1.0
		 * @since 10/05/2016
		 */
		public void editarProducto(RowEditEvent event) {

			Producto product = (Producto) event.getObject();
			System.out.println("modificando precio del producto....: "+product.getPrecio());
			logger.debug("modificando precio del producto....: "+product.getPrecio());
			
		}




		/**
		 * Cancela la modificaci�n
		 * 
		 * @param event
		 * @author jzapana
		 * @version 1.0
		 * @since 10/05/2016
		 */
		public void cancelarEdicion(RowEditEvent event) {
			//addLocalizedWarningMessage("bundle", "message.cancel.success");
			logger.debug("cancelando ....");
			System.out.println("cancelando....");
			
		}
		
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public Integer getproductoId() {
			return productoId;
		}
		public void setproductoId(Integer productoId) {
			this.productoId = productoId;
		}
		public List<Producto> getProductList() {
			return productoList;
		}
		public void setProductList(List<Producto> productList) {
			this.productoList = productList;
		}


		

}
